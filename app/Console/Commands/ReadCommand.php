<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\RobotController;

class ReadCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'execute:commands {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Read robot commands from a input file using a format given';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /**
         * Argument : File name
         */
        $file = $this->argument('file');
        $source = 'storage/data/'.$file;
        $handle = fopen($source, 'r');

        /**
         * New RobotController instance
         */
        $robot = new RobotController();
        $robot->execute($handle);
    }
}
