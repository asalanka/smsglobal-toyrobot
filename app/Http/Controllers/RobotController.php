<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\Movement;
use InvalidArgumentException;

class RobotController extends Controller
{
    use Movement;

    protected $x;
    protected $y;
    private $direction;

    protected $rotationMapRight = [
        'NORTH' => 'EAST',
        'EAST'  => 'SOUTH',
        'SOUTH' => 'WEST',
        'WEST'  => 'NORTH',
    ];

    protected $rotationMapLeft = [
        'NORTH' => 'WEST',
        'EAST'  => 'NORTH',
        'SOUTH' => 'EAST',
        'WEST'  => 'SOUTH',
    ];

    /**
     * @param $command
     */
    public function execute($handle){

        while (($command = fgets($handle))) {
            extract($this->parse($command));

            $x = str_replace(array("\n",    "\r"), '', $x);
            $y = str_replace(array("\n", "\r"), '', $y);
            $direction = str_replace(array("\n", "\r"), '', $direction);

            // Execute each robot method with arguments
            switch ($method) {
                case 'PLACE':
                    $this->place($x, $y, $direction);
                    break;

                case 'MOVE':
                    $this->move();
                    break;

                case 'LEFT':
                case 'RIGHT':
                    $this->rotate($method);
                    break;

                case 'REPORT':
                    echo $this->report();
                    break;
            }
        }

        fclose($handle);
    }

    /**
     * @param $data
     * @return array
     */
    protected function parse($data)
    {
        // Extract method and arguments from command
        $args = array('method' => NULL, 'x' => 0, 'y' => 0,'direction' => NULL);
        //get row data
        $data = $this->multiexplode(array(","," "), $data);

        $i = 0;
        if(!empty($args)){
            foreach ($args as $key => $value){
                if(isset($data[$i])){
                    $args[$key] = $data[$i];
                    $i++;
                }
            }
        }

        // Extract captured arguments with fallback defaults
        $method = trim($args['method']);
        $x = trim($args['x']);
        $y = trim($args['y']);
        $direction = trim($args['direction']);

        return compact('method','x', 'y', 'direction');
    }

    /**
     * @param $delimiters
     * @param $string
     * @return array
     */
    protected function multiexplode ($delimiters, $string) {

        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return  $launch;
    }

    /**
     * @param $x
     * @param $y
     * @param $direction
     */
    protected function place($x, $y, $direction){

        try{
            // Validate the movement with in permitted area
            if (!$this->isWithinBounds($x, $y)){
                throw new \Exception('Error occured, not within boundaries.');
            }

            // Validate the direction
            if (!$this->isValidDirection($direction)){
                throw new \Exception('Error occured, direction is not valid.'. $direction);
            }

            // If valid, assign robot coordinates and direction

            $this->x = $x;
            $this->y = $y;
            $this->direction = $direction;

        }catch(\Exception $e){
            print $e->getMessage()."\n";
        }
    }

    /**
     * Check whether robot has a assigned movement area.
     * @return bool
     */
    protected function isMovementAreaExists(){
        return (!is_null($this->x) && !is_null($this->y));
    }

    /**
     * Move robot from where its currently standing
     *
     */
    protected function move(){

        // Valid if robot movement exists
        if(!$this->isMovementAreaExists()){
            return;
        }

        // If valid, get current robot position
        $x = $this->x;
        $y = $this->y;
        $direction = $this->direction;

        // Calculate the movement based on direction
        switch ($direction) {
            case 'NORTH':
                $y += 1;
                break;

            case 'EAST':
                $x += 1;
                break;

            case 'SOUTH':
                $y -= 1;
                break;

            case 'WEST':
                $x -= 1;
                break;
        }

        if (!$this->isWithinBounds($x, $y)){
            return;
        }

        // Set new coordinates for the robot
        $this->x = $x;
        $this->y = $y;
    }

    protected function rotate($rotation){

        // Valid if robot movement exists
        if(!$this->isMovementAreaExists()){
            return;
        }

        $this->direction = $this->changeTheDirection($rotation);
    }

    protected function changeTheDirection($rotation){
        if(!$this->isValidRotation($rotation)){
            return;
        }

        if($rotation == 'RIGHT'){
            $result = isset($this->rotationMapRight[$this->direction]) ? $this->rotationMapRight[$this->direction] : null;
        }

        if($rotation == 'LEFT'){
            $result = isset($this->rotationMapLeft[$this->direction]) ? $this->rotationMapLeft[$this->direction] : null;
        }

        return $result;
    }

    public function report(){

        // Valid if robot movement exists
        if(!$this->isMovementAreaExists()){
            return;
        }

        print $this->x.','.$this->y.','.$this->direction."\n";
    }
}
