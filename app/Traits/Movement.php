<?php
namespace App\Traits;

trait Movement
{

    /**
     * Validate the movement coordinates
     * @param $x
     * @param $y
     * @return bool
     */

    protected function isWithinBounds($x, $y)
    {
        return (0 <= $x && $x < getenv('MOVEMENT_WIDTH')) && (0 <= $y && $y < getenv('MOVEMENT_HEIGHT'));
    }

    protected function isValidDirection($direction){
        //Able to maintain from env varible as well using a string concatanation
        $directionTypes = array('NORTH','EAST','SOUTH','WEST');

        return in_array($direction, $directionTypes);
    }

    protected function isValidRotation($rotation){
        //Able to maintain from env varible as well using a string concatanation
        $rotationTypes = array('LEFT','RIGHT');

        return in_array($rotation, $rotationTypes);
    }


}