# ReadMe Instructions
###### Development environment
-----------------------------
[Laravel 5.8 homestead environment with Vagrant VM](https://laravel.com/docs/5.8/homestead)
## How to run the solution

.env file variables:
 - please set the following environment variables within in .env file as follows:
 
```
MOVEMENT_WIDTH=5
MOVEMENT_HEIGHT=5
```

Command line argument : Filename E.g. **exampleA.txt**

There will be three test data files  exampleA.txt,  exampleB.txt and  exampleC.txt, 
you can pass individual file as a argument to execute a Laravel artisan command "ReadCommand" 
through the command prompt.
```
vagrant@homestead:/vagrant/code/toyrobot$ php artisan execute:commands exampleA.txt
vagrant@homestead:/vagrant/code/toyrobot$ php artisan execute:commands exampleB.txt
vagrant@homestead:/vagrant/code/toyrobot$ php artisan execute:commands exampleC.txt
```

## How to run the tests

There are two unit tests with functional tests created.
- **IsValidDataTest**
```phpunit --filter IsValidDataTest```
- **IsValidMovementTest**
```vagrant@homestead:/vagrant/code/toyrobot$ phpunit --filter IsValidMovementTest```
- **Run all the tests** 
```phpunit```
    or
```vagrant@homestead:/vagrant/code/toyrobot$ ./vendor/bin/phpunit```
###### Functional Test cases

- **testIsWithinBounds**
    > Test functionality to test whether movement is within the permitted area.
- **testIsValidDirection**
    > Test functionality to test whether movement is for a permitted direction.
- **testIsValidRotation**
    > Test functionality to test whether movement is for a permitted rotation type LEFT or RIGHT.
- **testIsValidData**
    > Test functionality to validate data, and the correct format
- **testIsParseValid**
    > Test functionality to if data is wrong parse functionality will validate and response correct response

## Assumptions
- There will be one robot at any given time (One Robot instance)

## Design
- Used Laravel command to initialize the robot and pass an argument (data), as Laravel commands are convenient to
pass arguments.
- Used one process controller to handle the robot and execute method to assign and switch between robot functionalities. 
- Used Laravel Trait as a abstract class where to use extendable functionalities of the Robot.

## Example input and outputs
-----------------------------
**Example a - exampleA.txt**
```
PLACE 0,0,NORTH
MOVE
REPORT
```
**Example a : results**
```0,1,NORTH```
----------------------------
**Example b - exampleB.txt**
```
PLACE 0,0,NORTH
LEFT
REPORT
```
**Example b : results**
```0,0,WEST```
-------------------------
**Example c - exampleC.txt**
```
PLACE 1,2,EAST
MOVE
MOVE
LEFT
MOVE
REPORT
```
**Example c : results**
```3,3,NORTH```
-------------------------