<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class IsValidDataTest extends TestCase
{
    /**
     * Test functionality to validate data, and the correct format
     *
     * @return void
     */
    public function testIsValidData()
    {
        //Sample test data (correct)
        $data = "PLACE 1,2,EAST";

        //Sample test data (incorrect)
        //$data = ",PLACE 1,2,EAST";

        $args = array('method' => NULL, 'x' => 0, 'y' => 0,'direction' => NULL);
        //get row data
        $data = $this->multiexplode(array(","," "), $data);

        $i = 0;
        if(!empty($args)){
            foreach ($args as $key => $value){
                if(isset($data[$i])){
                    $args[$key] = $data[$i];
                    $i++;
                }
            }
        }

        // Extract captured arguments with fallback defaults
        $method = trim($args['method']);
        $x = trim($args['x']);
        $y = trim($args['y']);
        $direction = trim($args['direction']);

        if(isset($method) && !empty($method)  && isset($x) && isset($y) && isset($direction)){
            return $this->assertTrue(true);
        }

        $this->assertTrue(false);
    }

    /**
     * Test functionality to if data is wrong parse functionality will validate and response correct response
     */
    public function testIsParseValid()
    {
        //Sample test data (incorrect)
        $data = ",PLACE 1,2,EAST";

        $args = array('method' => NULL, 'x' => 0, 'y' => 0,'direction' => NULL);
        //get row data
        $data = $this->multiexplode(array(","," "), $data);

        $i = 0;
        if(!empty($args)){
            foreach ($args as $key => $value){
                if(isset($data[$i])){
                    $args[$key] = $data[$i];
                    $i++;
                }
            }
        }

        // Extract captured arguments with fallback defaults
        $method = trim($args['method']);

        if(isset($method) && empty($method)){
            return $this->assertTrue(true);
        }

        $this->assertTrue(false);
    }

    /**
     * @param $delimiters
     * @param $string
     * @return array
     */
    protected function multiexplode ($delimiters, $string) {

        $ready = str_replace($delimiters, $delimiters[0], $string);
        $launch = explode($delimiters[0], $ready);
        return  $launch;
    }
}
