<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class IsValidMovementTest extends TestCase
{
    /**
     * Test functionality to test whether movement is within the permitted area.
     *
     * @return void
     */
    public function testIsWithinBounds()
    {
        //Sample test data (correct)
        $x = 3;
        $y = 3;

        //Sample test data (incorrect)
        //$x = 6;
        //$y = 6;

        if((0 <= $x && $x < getenv('MOVEMENT_WIDTH')) && (0 <= $y && $y < getenv('MOVEMENT_HEIGHT'))){
            return $this->assertTrue(true);
         }

        $this->assertTrue(false);
    }

    /**
     * Test functionality to test whether movement is for a permitted direction.
     */
    public function testIsValidDirection(){

        //Sample test data (correct)
        $direction = 'NORTH';

        //Sample test data (incorrect)
        //$direction = 'N0RTH';

        $directionTypes = array('NORTH','EAST','SOUTH','WEST');

        if(in_array($direction, $directionTypes)){
            return $this->assertTrue(true);
        }

        $this->assertTrue(false);
    }

    /**
     * Test functionality to test whether movement is for a permitted rotation type LEFT or RIGHT.
     */
    public function testIsValidRotation(){

        //Sample test data (correct)
        $rotation = 'LEFT';

        //Sample test data (incorrect)
        //$rotation = 'LEFTT';

        $rotationTypes = array('LEFT','RIGHT');

        if(in_array($rotation, $rotationTypes)){
            return $this->assertTrue(true);
        }

        $this->assertTrue(false);
    }
}
